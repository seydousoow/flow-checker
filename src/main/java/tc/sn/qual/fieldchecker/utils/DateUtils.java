package tc.sn.qual.fieldchecker.utils;

import lombok.extern.log4j.Log4j2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Log4j2
public final class DateUtils {

    public static LocalDate toLocalDate(String date) {
        if (date.equalsIgnoreCase("today")) return LocalDate.now();
        try {
            return LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
        } catch(DateTimeParseException e) {
            log.error("Error while parsing the date", e.getCause());
        }
        return null;
    }
}
