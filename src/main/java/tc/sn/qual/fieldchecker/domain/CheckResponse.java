package tc.sn.qual.fieldchecker.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CheckResponse {

    private Boolean status;
    private String message;

}
