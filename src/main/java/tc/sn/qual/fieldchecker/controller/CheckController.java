package tc.sn.qual.fieldchecker.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import tc.sn.qual.fieldchecker.domain.CheckResponse;
import tc.sn.qual.fieldchecker.services.FieldChecker;

@RestController
@RequiredArgsConstructor
public class CheckController {

    private final FieldChecker fieldChecker;

    @GetMapping(value = "/check", produces = "application/json")
    @ResponseBody
    public CheckResponse checkFormat(@RequestParam String field, @RequestParam String trans, @RequestParam String value) {
        return fieldChecker.checkFormat(field, trans, value);
    }
}
