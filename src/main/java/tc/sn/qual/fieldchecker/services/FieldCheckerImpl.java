package tc.sn.qual.fieldchecker.services;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.var;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import tc.sn.qual.fieldchecker.domain.CheckResponse;
import tc.sn.qual.fieldchecker.repository.FieldControlRepository;
import tc.sn.qual.fieldchecker.utils.DateUtils;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Log4j2
public class FieldCheckerImpl implements FieldChecker {

    private final FieldControlRepository repository;

    @Override
    public CheckResponse checkFormat(String fieldId, String transitionId, String value) {
        var field = repository.findByFieldIdEqualsAndTransitionIdEquals(fieldId, transitionId);
        if (field == null) return new CheckResponse(false, "Field not found! Check your parameters!");
        if (field.getRequired() && StringUtils.isBlank(value)) return new CheckResponse(false, "This field is required");
        if (!field.getRequired() && StringUtils.isBlank(value)) return new CheckResponse(true, "");

        JsonObject controls;
        JsonObject messages;
        try {
            var gson = new Gson();
            controls = gson.fromJson(field.getControls(), JsonObject.class);
            messages = gson.fromJson(field.getErrorMessage(), JsonObject.class);
        } catch (JsonSyntaxException e) {
            log.error("Error parsing the control to json", e.getCause());
            return new CheckResponse(false, "The syntax of the control of this field is wrong!");
        }
        return checkerChain(field.getFieldType(), value, controls, messages);
    }

    private CheckResponse checkerChain(String type, String value, @NotNull JsonObject controls, JsonObject messages) {
        if (type == null) return new CheckResponse(false, "The type of this field could not be queried");
        else if (!controls.keySet().isEmpty() && StringUtils.isBlank(value))
            return new CheckResponse(false, "This value could not be checked since it is empty");
        else if (controls.keySet().isEmpty())
            return new CheckResponse(true, "No Control is needed for this field");

        switch (type.toLowerCase()) {
            case "dates":
            case "date":
                return checkDates(value, controls, messages);
            case "text":
            case "string":
                return checkString(value, controls, messages);
            case "number":
                return checkNumber(value, controls, messages);
            case "list":
                return checkList(value, controls, messages);
            default:
                return new CheckResponse(false, "The type of this field has not been configured yet. Please report to your administrator!");
        }
    }

    @Contract("_, _, _ -> new")
    private @NotNull CheckResponse checkDates(String value, JsonObject controls, JsonObject messages) {
        LocalDate date = DateUtils.toLocalDate(value);
        if (date == null) return new CheckResponse(false, "This value is not a valid date");

        if (controls.has("EQ")) {
            var comparator = DateUtils.toLocalDate(controls.get("EQ").getAsString());
            if (comparator == null) return new CheckResponse(false, "This date specified in the control of this field is not a valid date");
            else if (!date.isEqual(comparator))
                return new CheckResponse(false, messages.has("EQ") ? messages.get("EQ").getAsString()
                        : "This date should be equals to ".concat(String.valueOf(comparator)));
        }

        if (controls.has("MIN") && date.isAfter(LocalDate.now().minusDays(controls.get("MIN").getAsInt())))
            return new CheckResponse(false, messages.has("MIN") ? messages.get("MIN").getAsString()
                    : "The date should be before ".concat(String.valueOf(LocalDate.now().plusDays(controls.get("MIN").getAsInt()))));

        if (controls.has("MAX") && date.isBefore(LocalDate.now().minusDays(controls.get("MAX").getAsInt())))
            return new CheckResponse(false, messages.has("MAX") ? messages.get("MAX").getAsString()
                    : "The date should be after ".concat(String.valueOf(LocalDate.now().minusDays(controls.get("MAX").getAsInt()))));

        return new CheckResponse(true, "");
    }

    @Contract("_, _, _ -> new")
    private @NotNull CheckResponse checkString(String value, @NotNull JsonObject controls, JsonObject messages) {
        if (controls.has("MINLENGTH") && value.length() < controls.get("MINLENGTH").getAsInt())
            return new CheckResponse(false, messages.has("MINLENGTH") ? messages.get("MINLENGTH").getAsString() :
                    "The value should have at least " + controls.get("MINLENGTH").getAsInt() + " characters!");

        if (controls.has("MAXLENGTH") && value.length() > controls.get("MAXLENGTH").getAsInt())
            return new CheckResponse(false, messages.has("MAXLENGTH") ? messages.get("MAXLENGTH").getAsString() :
                    "The value should not have more than " + controls.get("MAXLENGTH").getAsInt() + " characters!");

        if (controls.has("REG")) {
            try {
                if (!value.matches(controls.get("REG").getAsString()))
                    return new CheckResponse(false, messages.has("REG") ? messages.get("REG").getAsString() : "The text does not match the regex!");
            } catch (PatternSyntaxException e) {
                log.error("Could not check the regex, because it format is invalid!");
                return new CheckResponse(false, "This field could not be checked because the regex is invalid");
            }
        }

        return new CheckResponse(true, "");
    }

    @Contract("_, _, _ -> new")
    private @NotNull CheckResponse checkNumber(String value, JsonObject controls, JsonObject messages) {
        int number;
        try {
            number = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            log.error("Not a valid number: " + e.getMessage());
            return new CheckResponse(false, "This is not a valid number!");
        }

        if (controls.has("GT") && NumberUtils.compare(number - 1, controls.get("GT").getAsInt()) < 0)
            return new CheckResponse(false, messages.has("GT") ? messages.get("GT").getAsString() : "Should be greater than " + controls.get("GT"));
        
        if (controls.has("LT") && NumberUtils.compare(number + 1, controls.get("LT").getAsInt()) > 0)
            return new CheckResponse(false, messages.has("LT") ? messages.get("LT").getAsString() : "Should be less than " + controls.get("LT"));

        if (controls.has("EQ") && NumberUtils.compare(number, controls.get("EQ").getAsInt()) != 0)
            return new CheckResponse(false, messages.has("EQ") ? messages.get("EQ").getAsString() : "Should be equal to " + controls.get("EQ"));

        return new CheckResponse(true, "");
    }

    @Contract("_, _, _ -> new")
    private @NotNull CheckResponse checkList(String value, @NotNull JsonObject controls, JsonObject messages) {
        var items = new HashSet<>();
        if (!Arrays.stream(value.split(",")).filter(n -> !items.add(n)).collect(Collectors.toSet()).isEmpty())
            return new CheckResponse(false, "The list cannot contains duplicated values");

        if (controls.has("IN") && !Arrays.asList(controls.get("IN").getAsString().split(",")).containsAll(Arrays.asList(value.split(","))))
            return new CheckResponse(false, messages.has("IN") ? messages.get("IN").getAsString() : "Some items are not valid for this field!");

        if (controls.has("MIN") && value.split(",").length < controls.get("MIN").getAsInt())
            return new CheckResponse(false,
                    messages.has("MIN") ? messages.get("MIN").getAsString() : "Field should have at least " + controls.get("MIN").getAsInt() + " items!");
        if (controls.has("MAX") && value.split(",").length > controls.get("MAX").getAsInt())
            return new CheckResponse(false,
                    messages.has("MAX") ? messages.get("MAX").getAsString() : "max of " + controls.get("MAX").getAsInt() + " item reached!");

        return new CheckResponse(true, "");
    }

}
