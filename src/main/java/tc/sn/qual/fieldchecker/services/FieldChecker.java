package tc.sn.qual.fieldchecker.services;

import tc.sn.qual.fieldchecker.domain.CheckResponse;

public interface FieldChecker {

    CheckResponse checkFormat(String fieldId, String transitionId, String value);
}
