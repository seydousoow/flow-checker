package tc.sn.qual.fieldchecker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tc.sn.qual.fieldchecker.domain.FieldControl;

@Repository
public interface FieldControlRepository extends JpaRepository<FieldControl, Long> {

	FieldControl findByFieldIdEqualsAndTransitionIdEquals(String fieldId, String transitionId);

}
