package tc.sn.qual.fieldchecker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FieldCheckerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FieldCheckerApplication.class, args);
    }

}
